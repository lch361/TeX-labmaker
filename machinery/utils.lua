---@diagnostic disable:undefined-global
local M = {}

Macros = {}

local function argc_to_str(argc)
	if argc == 0 then
		return "", ""
	end

	-- local defstr = "#"..argc
	local defstr = {}
	for i = 1,argc do
		defstr[#defstr+1] = "#"..i
	end

	local multiple = { "'#1'" }
	for i = 2,argc do
		multiple[#multiple+1] = ","
		multiple[#multiple+1] = table.concat{ "'#", i, "'" }
	end

	return table.concat(defstr), table.concat(multiple)
end

M.def_macro = function(name, argc, func)
	local argc_def_str, arg_str = argc_to_str(argc)
	Macros[name] = func
	tex.sprint(table.concat {
		"\\def\\", name, argc_def_str, "{",
			"\\directlua{Macros[\"", name, "\"](", arg_str, ")}",
		"}"
	})
end

M.undef = function(name)
	Macros[name] = nil
	tex.sprint(table.concat {
		"\\def\\", name, "{",
			"\\directlua{tex.error(\"Called undefined macro ", name, "\")}",
		"}"
	})
end

M.def = function (modname)
	local m = require("machinery.macros."..modname)
	M.def_macro(modname, m.argc, m.func)
end

M.get_pageno = function ()
	return tex.count[0]
end

M.set_pageno = function (no)
	tex.count[0] = no
end

return M
