---@diagnostic disable:undefined-global
local page_style = require("machinery.page-style")
page_style.setup()

local utils = require("machinery.utils")
utils.def("ImageSetup")
utils.def("FontsSetup")
utils.def("Begin")
