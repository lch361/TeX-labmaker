---@diagnostic disable:undefined-global
local config = require("machinery.config")

local M = {}

M.referenced = {}

local next_image_num = 1

M.put_image = function (filename, size_mul)
	local image_fixed = img.scan({filename = filename})
	local image = img.copy(image_fixed)

	local hw_ratio = image.height / image.width
	image.width  = tex.hsize * size_mul
	image.height = tex.hsize * size_mul * hw_ratio

	local margin = (tex.hsize - image.width) / 2

	local image_node = img.node(image)

	local lglue = node.new("glue")
    lglue.width = margin

	lglue.next = image_node
	image_node.prev = lglue

	local hbox = node.hpack(lglue)

	local vglue = node.new("glue")
    vglue.width = config.imageskip

	vglue.next = hbox
	hbox.prev = vglue

	local vbox = node.vpack(vglue)

	node.write(vbox)
end

M.reference_image = function (name)
	if M.referenced[name] == nil then
		M.referenced[name] = next_image_num
		local result = next_image_num
		next_image_num = next_image_num + 1
		return result
	else
		return M.referenced[name]
	end
end

return M
