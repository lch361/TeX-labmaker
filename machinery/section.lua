local font  = require("machinery.font")
local utils = require("machinery.utils")

local M = {}

M.start_section_contents = function ()
	tex.print("\\"..font.main_font_names.serif)

	utils.def("BulletListBegin")
	utils.def("NumberListBegin")
	utils.def("InsertImage")
	utils.def("RefImage")

	utils.undef("End")
end

M.end_section_contents = function ()
	tex.print("\\vfill")
	tex.print("\\eject")

	utils.def("End")

	utils.undef("BulletListBegin")
	utils.undef("NumberListBegin")
	utils.undef("InsertImage")
	utils.undef("RefImage")
end

M.state = {
	section_num = 1,
	subsection_nums = {},
}

-- returns a section number
M.section_begin = function ()
	local num = M.state.section_num

	M.state.subsection_nums = { 1 }
	return num
end

M.section_end = function ()
	M.state.section_num = M.state.section_num + 1
	M.state.subsection_nums = {}
end

M.subsection_begin = function ()
	local result_num = { M.state.section_num }
	for _, v in ipairs(M.state.subsection_nums) do
		result_num[#result_num+1] = v
	end

	M.state.subsection_nums[#M.state.subsection_nums + 1] = 1

	return result_num
end

M.subsection_num_to_str = function (numlist)
	local result = { tostring(numlist[1]) }
	for i = 2,#numlist do
		result[#result+1] = "."
		result[#result+1] = tostring(numlist[i])
	end
	return table.concat(result)
end

-- Returns true if all subsections are ended
M.subsection_end = function ()
	M.state.subsection_nums[#M.state.subsection_nums] = nil
	local len = #M.state.subsection_nums
	M.state.subsection_nums[len] = M.state.subsection_nums[len] + 1

	return len == 1
end

M.refine_name = function (raw)
	local result = raw
	result = string.gsub(result, "{", "\\BracketOpen ")
	result = string.gsub(result, "}", "\\BracketClose ")
	return result
end

return M
