-- All requirements read from here: https://guap.ru/regdocs/docs/uch
local dimens = require("machinery.dimens")

local M = {}

-- Set to A4
M.page_size = {
	width  = 8.27  * dimens.inch,
	height = 11.69 * dimens.inch,
}

-- Set according to 2.4 requirement
M.margins = {
	top    = 2   * dimens.cm,
	bottom = 2   * dimens.cm,
	left   = 3   * dimens.cm,
	right  = 1.5 * dimens.cm,
}

M.font_styles = {
	serif = {
		family = "serif",
		variant = "regular",
		size = 12 * dimens.pt,
	},
	sans = {
		family = "sans",
		variant = "regular",
		size = 12 * dimens.pt,
	},
	mono = {
		family = "mono",
		variant = "regular",
		size = 12 * dimens.pt,
	},
	section = {
		family = "serif",
		variant = "bold",
		size = 14 * dimens.pt,
	},
	subsection = {
		family = "serif",
		variant = "bold",
		size = 13 * dimens.pt,
	},
}

M.parindent    = 1.25 * dimens.cm
M.baselineskip = M.font_styles.serif.size * 1.5

M.imageskip = 6 * dimens.pt

-- Given in 'chars'
M.contents_indent = 2
M.starting_pagenum = 3

return M
