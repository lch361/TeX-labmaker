local utils = require("machinery.utils")

local M = {}

local function draw_bullet(num)
	_ = num
	return "$\\bullet$"
end

local function draw_num(num)
	return num.."."
end

M.draw_item = {
	bullet = draw_bullet,
	number = draw_num,
}

M.state = {
	baseindent = tex.parindent,
	func_stack = {},
	elem_stack = {},
}

M.list_begin = function(is_bullet)
	local func_choice
	if is_bullet then
		func_choice = M.draw_item.bullet
	else
		func_choice = M.draw_item.number
	end

	if #M.state.func_stack > 0 then
		tex.parindent = tex.parindent + M.state.baseindent
	else
		utils.def("Item")
		utils.def("ListEnd")
	end
	M.state.func_stack[#M.state.func_stack+1] = func_choice
	M.state.elem_stack[#M.state.elem_stack+1] = 1
end

M.list_end = function()
	if #M.state.func_stack == 0 then return end

	M.state.func_stack[#M.state.func_stack] = nil
	M.state.elem_stack[#M.state.elem_stack] = nil

	if #M.state.func_stack > 0 then
		tex.parindent = tex.parindent - M.state.baseindent
	else
		utils.undef("Item")
		utils.undef("ListEnd")
	end
end

M.list_item = function ()
	local num  = M.state.elem_stack[#M.state.elem_stack]
	local func = M.state.func_stack[#M.state.func_stack]

	M.state.elem_stack[#M.state.elem_stack] = num + 1
	return func(num)
end

return M
