local M = {}

M.image = {
	dir = "pictures",
	ext = "png",
}

M.fonts = {
	dir = "fonts",
	ext = "ttf",
}

return M
