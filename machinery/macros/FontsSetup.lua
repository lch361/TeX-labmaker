local setup = require("machinery.setup")

local function func(dirname, ext)
	setup.fonts.dir = dirname
	setup.fonts.ext = ext
end

return {
	argc = 2,
	func = func
}
