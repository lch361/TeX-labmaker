---@diagnostic disable:undefined-global
local utils    = require("machinery.utils")
local font     = require("machinery.font")
local section  = require("machinery.section")
local contents = require("machinery.contents")

local function func()
	tex.print(string.format("\\centerline{\\%s ВВЕДЕНИЕ}", font.main_font_names.section))
	section.start_section_contents()
	contents.add_before("Введение")

	-- This is a one-time call
	utils.undef("IntroductionBegin")
	utils.def("IntroductionEnd")
end

return {
	argc = 0,
	func = func
}
