local utils   = require("machinery.utils")
local section = require("machinery.section")

local function func()
	section.section_end()
	section.end_section_contents()

	utils.undef("SectionEnd")
	utils.def("SectionBegin")
end

return {
	argc = 0,
	func = func
}
