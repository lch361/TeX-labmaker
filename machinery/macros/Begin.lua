local utils = require("machinery.utils")
local font  = require("machinery.font")
local setup = require("machinery.setup")

local function func()
	utils.undef("FontsSetup")
	utils.undef("ImageSetup")

	-- Undef self, making this a one-time macro
	utils.undef("Begin")

	-- Only now the final macro becomes accessible
	utils.def("End")

	-- Setup fonts
	font.fonts_setup(setup.fonts.dir, setup.fonts.ext)

	-- Define available sections
	utils.def("IntroductionBegin")
	utils.def("SectionBegin")
	utils.def("ConclusionBegin")

	-- Define basic macros
	tex.print("\\def\\BracketOpen{{\\Uchar 123}}")
	tex.print("\\def\\BracketClose{{\\Uchar 125}}")
	tex.print("\\def\\Underline{{\\Uchar 95}}")
end

return {
	argc = 0,
	func = func
}
