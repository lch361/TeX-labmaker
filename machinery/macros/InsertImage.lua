---@diagnostic disable:undefined-global
local setup = require("machinery.setup")
local image = require("machinery.image")

local function func(name, description, size)
	local filename = table.concat { setup.image.dir, "/", name, ".", setup.image.ext }
	local description_str = ""
	if description ~= "" then
		description_str = " - "..description
	end

	local num = image.referenced[name]
	if num == nil then
		tex.error("Unreferenced image inserted.")
	end

	image.put_image(filename, tonumber(size))
	tex.print("")
	tex.print(string.format("\\centerline{\\Serif Рисунок %i %s}", num, description_str))
end

return {
	argc = 3,
	func = func
}
