local utils   = require("machinery.utils")
local section = require("machinery.section")

local function func()
	if section.subsection_end() then
		utils.undef("SubsectionEnd")
	end
end

return {
	argc = 0,
	func = func
}
