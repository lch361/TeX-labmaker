---@diagnostic disable:undefined-global
local utils    = require("machinery.utils")
local font     = require("machinery.font")
local section  = require("machinery.section")
local contents = require("machinery.contents")

local function func()
	tex.print(string.format("\\centerline{\\%s ЗАКЛЮЧЕНИЕ}", font.main_font_names.section))
	section.start_section_contents()
	contents.add_after("Заключение")

	-- Would be pointless to come back to these
	utils.undef("IntroductionBegin")
	utils.undef("SectionBegin")

	-- This is a one-time call
	utils.undef("ConclusionBegin")
	utils.def("ConclusionEnd")
end

return {
	argc = 0,
	func = func
}
