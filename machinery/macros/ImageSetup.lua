local setup = require("machinery.setup")

local function func(dirname, ext)
	setup.image.dir = dirname
	setup.image.ext = ext
end

return {
	argc = 2,
	func = func
}
