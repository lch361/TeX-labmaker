local list  = require("machinery.list")

local function func()
	list.list_begin(false)
end

return {
	argc = 0,
	func = func
}
