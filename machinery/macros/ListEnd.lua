local list  = require("machinery.list")

local function func()
	list.list_end()
end

return {
	argc = 0,
	func = func
}
