local utils   = require("machinery.utils")
local section = require("machinery.section")

local function func()
	section.end_section_contents()

	utils.undef("ConclusionEnd")
end

return {
	argc = 0,
	func = func
}
