local contents = require("machinery.contents")

local function func()
	contents.make_contents_page()
	tex.sprint("\\end")
end

return {
	argc = 0,
	func = func
}
