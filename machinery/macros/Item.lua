local list  = require("machinery.list")

local function func()
	local result = list.list_item()
	tex.sprint(string.format("\\item{%s} ", result))
end

return {
	argc = 0,
	func = func
}
