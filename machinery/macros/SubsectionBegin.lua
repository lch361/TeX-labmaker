local utils    = require("machinery.utils")
local font     = require("machinery.font")
local config   = require("machinery.config")
local section  = require("machinery.section")
local contents = require("machinery.contents")

local function func(name)
	local nums = section.subsection_begin()
	local nums_str = section.subsection_num_to_str(nums)

	local indent = (#nums - 1) * config.contents_indent
	contents.add_section(table.concat { nums_str, " ", name }, indent)

	tex.print(string.format("{\\%s %s %s\\par}", font.main_font_names.subsection, nums_str, section.refine_name(name)))

	utils.def("SubsectionEnd")
end

return {
	argc = 1,
	func = func
}
