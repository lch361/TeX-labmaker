local utils    = require("machinery.utils")
local font     = require("machinery.font")
local section  = require("machinery.section")
local contents = require("machinery.contents")

local function func(name)
	local num = section.section_begin()
	tex.print(string.format("{\\%s %i. %s\\par}", font.main_font_names.section, num, section.refine_name(name)))
	section.start_section_contents()
	contents.add_section(table.concat { tostring(num), ". ", name }, 0)

	-- Would be pointless to do an introduction at this moment
	utils.undef("IntroductionBegin")

	-- Can only exit, unable to start nested sections
	utils.undef("SectionBegin")
	utils.def("SectionEnd")

	utils.def("SubsectionBegin")
end

return {
	argc = 1,
	func = func
}
