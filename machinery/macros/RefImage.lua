local image = require("machinery.image")

local function func(name)
	tex.sprint(tostring(image.reference_image(name)))
end

return {
	argc = 1,
	func = func
}
