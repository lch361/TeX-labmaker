---@diagnostic disable:undefined-global
local config = require("machinery.config")
local dimens = require("machinery.dimens")
local utils  = require("machinery.utils")

local M = {}

M.setup = function()
	pdf.setorigin(0, 0) -- For some reason, this function ignores 2nd arg

	tex.pagewidth  = config.page_size.width
	tex.pageheight = config.page_size.height

	tex.hoffset = config.margins.left
	tex.voffset = config.margins.top

	tex.hsize = config.page_size.width  - config.margins.left - config.margins.right
	tex.vsize = config.page_size.height - config.margins.top  - config.margins.bottom

	-- text style --

	tex.parindent    = config.parindent
	tex.baselineskip = config.baselineskip

	tex.emergencystretch = 1 * dimens.inch

	tex.print("\\raggedbottom") -- avoid page stretching

	-- Sets the pageno to 3 since the title list is skipped and there are also contents
	utils.set_pageno(config.starting_pagenum)
end

return M
