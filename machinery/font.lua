local config = require("machinery.config")
local dimens = require("machinery.dimens")

local M = {}

M.main_font_names = {
	serif = "Serif",
	sans  = "Sans",
	mono  = "Mono",
	section    = "FontSection",
	subsection = "FontSubsection",
}

M.load_define_font = function(filename, name, size)
	local pt_size = size / dimens.pt
	tex.print(table.concat {
		"\\font\\", name, " = {file:", filename, "} at ", pt_size, "pt"
	})
end

M.fonts_setup = function(fonts_dir, fonts_ext)
	tex.print("\\input luaotfload.sty")

	local styles = config.font_styles
	for k, v in pairs(styles) do
		local path = table.concat {
			fonts_dir, "/", v.family, "/", v.variant, ".", fonts_ext
		}
		local name = M.main_font_names[k]
		M.load_define_font(path, name, v.size)
	end
end

return M
