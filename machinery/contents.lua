---@diagnostic disable:undefined-global
local font   = require("machinery.font")
local utils  = require("machinery.utils")

local M = {}

M.before_sections = {}
M.sections = {}
M.after_sections = {}

local function print_linear(list)
	for _, v in ipairs(list) do
		tex.print(table.concat { "\\leftline{", v.name, "\\dotfill ", v.no, "}" })
	end
end

local function print_section(section)
	-- local indent_str = string.rep("\\ ", section.indent)
	local indent_cm = section.indent * 0.25
	local indent_str = string.format("\\hskip %f cm", indent_cm)
	tex.print(table.concat { "\\leftline{", indent_str, section.name, "\\dotfill ", section.no, "}" })
end

M.add_before = function (name)
	M.before_sections[#M.before_sections+1] = {
		name = name,
		no = utils.get_pageno(),
	}
end

M.add_section = function (name, indent)
	M.sections[#M.sections+1] = {
		name = name,
		no = utils.get_pageno(),
		indent = indent,
	}
end

M.add_after = function (name)
	M.after_sections[#M.after_sections+1] = {
		name = name,
		no = utils.get_pageno(),
	}
end

M.make_contents_page = function ()
	utils.set_pageno(2)
	tex.print(table.concat { "\\centerline{\\", font.main_font_names.section, " СОДЕРЖАНИЕ}" })

	print_linear(M.before_sections)

	for _, v in ipairs(M.sections) do
		print_section(v)
	end

	print_linear(M.after_sections)

	tex.print("\\vfill")
	tex.print("\\eject")
end

return M
