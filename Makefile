TEX := luatex
FILEBASENAME := report
RESULTDIR := result

default:
	FILEBASENAME="$(FILEBASENAME)" RESULTDIR="$(RESULTDIR)" exec ./make.sh

clean:
	rm -rf $(RESULTDIR)/*

.PHONY: default clean
