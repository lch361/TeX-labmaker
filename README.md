# TeX labmaker

- Are you studying in university?
- Do you hate GOST or don't have time to read it?
- Are you tired of the WYSIWYG pattern, or, to put it simply, when transforming one element in LibreOffice changes the look of whole document immediately?
- Are you tired of going through the whole document to fix a minor style issue occuring multiple times?

Then fear not: the TeX labmaker is for you!

# Features

- TeX-based document editing. In fact, it's just a set of macros which do the job.
- Edit the contents of your work without distraction on style. Macros will render your document in GOST-appropriate style.
- Automatic "Table of contents" generation
- Unicode, ttf/otf fonts support and png/jpg image loading

# Requirements

- Linux system: tested only on it
- Poppler: for `pdfunite` and `pdfseparate` utils
- LuaTeX: the chosen TeX engine itself.

# How to use

1.  Write your report in file `report.tex`
2.  `make`
3.  Your report was rendered at `result/report.pdf`. Enjoy :)
