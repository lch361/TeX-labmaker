#!/usr/bin/env sh

TEX=luatex
PAGESDIR=pages

main()
{
	mkdir -p "${RESULTDIR}"
	${TEX} --output-directory="${RESULTDIR}" "${FILEBASENAME}.tex"

	cd "${RESULTDIR}"
	mkdir -p "${PAGESDIR}"
	pdfseparate report.pdf "${PAGESDIR}/%d"

	cd "${PAGESDIR}"
	local LASTPAGENUM="`ls | wc -l`"
	local PDFUNITE_ARGS="${LASTPAGENUM}"
	for i in `seq 1 $(expr ${LASTPAGENUM} - 1)`
	do
		PDFUNITE_ARGS="${PDFUNITE_ARGS} ${i}"
	done

	pdfunite $PDFUNITE_ARGS ../report.pdf

	cd "${OLDPWD}"
	rm -rf "${PAGESDIR}"
}

main
